# Create Entities
typeorm-model-generator -h DB_HOST -d DB_NAME -u DB_USER -x DB_PASS -e postgres --noConfig -o ./src/entities --case-file none

# Instruction
## Running On Local
- `git clone [this-repository].git` clone repository
- `yarn install` install package.json package
- `cp .env.example .env` create new env
- Create new postgres database and run script `database.sql` in /migrations
- Create init data for database. Import `main_data.xlsx` in /migrations
- `yarn start:dev` start the app
- Import `postman_collection.json` in /migrations for example postman collection

## Auth
- All endpoint protected by JWT Token & Role based, except endpoint Get Token
- Get JWT Token from `/api/auth/token` with Basic Auth. username = admin, password = admin
- Put JWT Token to headers as Bearer
- Do request to endpoint

## Change Repository
- `rm -rf /.git` remove git directory
- `git init` create new git directory
- `git remote add origin [new-repository]`
- `git checkout -b develop` change to branch develop
- `git push origin develop` push to branch develop
- `git push origin develop:master` push from branch develop to master (mirror push)

# Create Module
- `nest generate resource common/[modulename] --no-spec`
- `nest generate interceptor shared/interceptors/[interceptor-name] --no-spec`
- `nest generate guard shared/guards/[guard-name]`
- `nest generate filter shared/filters/[filter-name]`

# Note
- `/migrations` used for initialization app first time
- `/src/main.ts` entrypoint app
- `/src/routes.ts` list all routes for modules
- `/src/common` module directory.
- `/src/common/[module]/[module-name].controller.ts` controller for handle request & response API
- `/src/common/[module]/[module-name].service.ts` method for data manipulation
- `/src/common/[module]/[module-name].dto.ts` type Data Transfer Object for validation request or response structure
- `/src/common/[module]/[module-name].module.ts` wrapper for service, controller, entities and other module
- `/src/configs` configuration folder like database connection and other config
- `/src/entities` model or schema object for database
- `/src/shared` global directory for share function between module
- `/src/shared/enum` global enumeration
- `/src/shared/interceptor` interceptor class for request or response
- `/src/shared/guards` guards for auth like authentication middleware
- `/src/shared/filters` Filter exception for HTTP response