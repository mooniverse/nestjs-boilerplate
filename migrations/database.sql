CREATE TYPE "supplier_price_type" AS ENUM (
  'FIXED',
  'BILL'
);

CREATE TYPE "product_calculation_type" AS ENUM (
  'FIXED_PRICE',
  'BASED_PRICE',
  'REMOVE_PRICE'
);

CREATE TYPE "transaction_status_type" AS ENUM (
  'CREATED',
  'INQUIRY',
  'ON_ROUTING',
  'ON_PROCESS',
  'FAILED',
  'SUCCESS'
);

CREATE TABLE "channels" (
  "channel_id" varchar(25) PRIMARY KEY,
  "channel_name" varchar(255) NOT NULL,
  "is_active" boolean NOT NULL,
  "cif_number" varchar(30) UNIQUE NOT NULL,
  "callback_url" varchar(255) NOT NULL,
  "create_time" timestamp NOT NULL DEFAULT (now())
);

CREATE TABLE "channel_account_balances" (
  "channel_account_balance_id" varchar(25) PRIMARY KEY,
  "channel_id" varchar(25) UNIQUE NOT NULL,
  "account_number" varchar(30) UNIQUE NOT NULL
);

CREATE TABLE "channel_account_vas" (
  "channel_account_va_id" varchar(25) PRIMARY KEY,
  "channel_account_balance_id" varchar(25),
  "vendor" varchar(75),
  "vendor_ref_id" varchar(50) NOT NULL,
  "is_active" boolean NOT NULL,
  "va_number" varchar(75) NOT NULL,
  "va_type" varchar(50) NOT NULL,
  "va_status" varchar(50) NOT NULL,
  "amount" int8 DEFAULT 0,
  "create_time" timestamp NOT NULL DEFAULT (now()),
  "create_by" varchar(25),
  "paid_time" timestamp,
  "expire_time" timestamp,
  "data" jsonb DEFAULT '{}'
);

CREATE TABLE "user_roles" (
  "user_role_id" varchar(25) PRIMARY KEY,
  "role" varchar(100)
);

CREATE TABLE "users" (
  "user_id" varchar(25) PRIMARY KEY,
  "username" varchar(100) UNIQUE NOT NULL,
  "password" varchar(255) NOT NULL,
  "api_key" varchar(255),
  "api_secret" varchar(255),
  "channel_id" varchar(25) NOT NULL,
  "user_role_id" varchar(25) NOT NULL,
  "user_fullname" varchar(255),
  "create_time" timestamp NOT NULL DEFAULT (now()),
  "create_by" varchar(25),
  "update_time" timestamp NOT NULL DEFAULT (now()),
  "update_by" varchar(25)
);

CREATE TABLE "suppliers" (
  "supplier_id" varchar(25) PRIMARY KEY,
  "supplier_alias" varchar(50) UNIQUE NOT NULL,
  "supplier_name" varchar(255),
  "is_active" boolean NOT NULL,
  "create_time" timestamp NOT NULL DEFAULT (now()),
  "additional_data" jsonb DEFAULT '{}'
);

CREATE TABLE "product_categories" (
  "product_category_id" varchar(25) PRIMARY KEY,
  "category_alias" varchar(75) UNIQUE NOT NULL,
  "category_name" varchar(150) NOT NULL,
  "create_time" timestamp NOT NULL DEFAULT (now())
);

CREATE TABLE "product_main" (
  "product_main_id" varchar(25) PRIMARY KEY,
  "product_name" varchar(255) NOT NULL,
  "product_category_id" varchar(25) NOT NULL,
  "is_active" boolean NOT NULL,
  "is_single_payment" boolean NOT NULL,
  "mandatory_fields" jsonb DEFAULT '{}',
  "description" varchar(255) NOT NULL,
  "create_time" timestamp DEFAULT (now()),
  "create_by" varchar(25),
  "update_time" timestamp DEFAULT (now()),
  "update_by" varchar(25)
);

CREATE TABLE "product_suppliers" (
  "product_supplier_id" varchar(25) PRIMARY KEY,
  "supplier_id" varchar(25) NOT NULL,
  "product_main_id" varchar(25) NOT NULL,
  "product_supplier_ref_id" varchar(255) NOT NULL,
  "product_supplier_name" varchar(255) NOT NULL,
  "is_active" boolean DEFAULT true,
  "buying_price" int4 NOT NULL DEFAULT 0,
  "admin_price" int4 NOT NULL DEFAULT 0,
  "admin_commission" int4 NOT NULL DEFAULT 0,
  "margin_of_admin" int4 NOT NULL DEFAULT 0,
  "description" varchar(255),
  "create_time" timestamp DEFAULT (now()),
  "create_by" varchar(25),
  "update_time" timestamp DEFAULT (now()),
  "update_by" varchar(25)
);

CREATE TABLE "product_channels" (
  "product_channels_id" varchar(25) PRIMARY KEY,
  "custom_product_id" varchar(100),
  "channel_id" varchar(25) NOT NULL,
  "product_main_id" varchar(25) NOT NULL,
  "is_active" boolean DEFAULT true,
  "create_time" timestamp DEFAULT (now()),
  "create_by" varchar(25),
  "update_time" timestamp DEFAULT (now()),
  "update_by" varchar(25),
  "price_config" jsonb DEFAULT '[{"key":"selling_price","value":"0","name":"Sell Price","mandatory_fields":true,"adjust_to_channel":true,"adjust_to_customer":true,"show_to_receipt":true,"calculation_type":"BASED_PRICE"},{"key":"admin","value":"0","name":"Admin","mandatory_fields":true,"adjust_to_channel":true,"adjust_to_customer":true,"show_to_receipt":true,"calculation_type":"BASED_PRICE"},{"key":"customadmin","value":"2500","name":"Custom Admin","mandatory_fields":false,"adjust_to_channel":false,"adjust_to_customer":true,"show_to_receipt":true,"calculation_type":"FIXED_PRICE"}]'
);

CREATE TABLE "transactions" (
  "transaction_id" varchar(25) PRIMARY KEY,
  "channel_id" varchar(25) NOT NULL,
  "channel_reference" varchar(255) NOT NULL,
  "product_channels_id" varchar(25) NOT NULL,
  "buying_price" int4 NOT NULL,
  "price_channel" int4 NOT NULL,
  "price_customer" int4 NOT NULL,
  "net_margin_platform" int4 NOT NULL,
  "transaction_status" transaction_status_type NOT NULL,
  "create_time" timestamp DEFAULT (now()),
  "create_by" varchar(25),
  "customer_data" jsonb DEFAULT '{}',
  "payment_reference" varchar(50),
  "is_reversal" boolean NOT NULL DEFAULT false,
  "callback_channel" jsonb DEFAULT '{}',
  "receipt_customer" jsonb DEFAULT '{}',
  "current_route_id" varchar(25) UNIQUE
);

CREATE TABLE "transaction_routes" (
  "transaction_route_id" varchar(25) PRIMARY KEY,
  "transaction_id" varchar(25) NOT NULL,
  "route_number" int4 NOT NULL DEFAULT 1,
  "supplier_id" varchar(25) NOT NULL,
  "supplier_reference" varchar(100),
  "transaction_route_status" transaction_status_type,
  "create_time" timestamp DEFAULT (now()),
  "data" jsonb DEFAULT '{}',
  "callback_supplier" jsonb DEFAULT '{}',
  "is_success" boolean DEFAULT false
);

CREATE TABLE "rc_main" (
  "rc_main_id" varchar(25) PRIMARY KEY,
  "rc" varchar(20) UNIQUE,
  "message" varchar(255)
);

CREATE TABLE "rc_suppliers" (
  "rc_supplier_id" varchar(25) PRIMARY KEY,
  "supplier_id" varchar(25),
  "rc_supplier" varchar(20),
  "message" varchar(255),
  "reroute" boolean
);

CREATE TABLE "rc_mappers" (
  "rc_mapper_id" varchar(25) PRIMARY KEY,
  "rc_main_id" varchar(25),
  "rc_supplier_id" varchar(25)
);

CREATE TABLE "app_configs" (
  "key" varchar(75) PRIMARY KEY,
  "value" varchar(500)
);

CREATE UNIQUE INDEX ON "users" ("username");

CREATE INDEX ON "users" USING BTREE ("user_fullname");

CREATE INDEX ON "product_categories" USING HASH ("category_alias");

CREATE INDEX ON "product_main" USING BTREE ("product_name");

CREATE INDEX ON "product_main" USING HASH ("is_single_payment");

CREATE INDEX ON "product_suppliers" USING HASH ("supplier_id");

CREATE INDEX ON "product_suppliers" USING HASH ("is_active");

CREATE UNIQUE INDEX ON "product_channels" ("custom_product_id", "channel_id");

CREATE INDEX ON "product_channels" USING HASH ("channel_id");

CREATE INDEX ON "product_channels" USING HASH ("is_active");

CREATE UNIQUE INDEX ON "transactions" ("channel_id", "channel_reference");

CREATE INDEX ON "transactions" USING BTREE ("channel_id");

CREATE INDEX ON "transactions" USING BTREE ("channel_reference");

CREATE INDEX ON "transactions" USING BTREE ("product_channels_id");

CREATE INDEX ON "transactions" USING BTREE ("transaction_status");

CREATE UNIQUE INDEX ON "transaction_routes" ("transaction_id", "route_number");

CREATE UNIQUE INDEX ON "transaction_routes" ("transaction_id", "supplier_id");

CREATE UNIQUE INDEX ON "transaction_routes" ("supplier_id", "supplier_reference");

CREATE INDEX ON "transaction_routes" USING HASH ("transaction_id");

CREATE INDEX ON "transaction_routes" USING HASH ("is_success");

ALTER TABLE "channel_account_balances" ADD FOREIGN KEY ("channel_id") REFERENCES "channels" ("channel_id");

ALTER TABLE "channel_account_vas" ADD FOREIGN KEY ("channel_account_balance_id") REFERENCES "channel_account_balances" ("channel_account_balance_id");

ALTER TABLE "users" ADD FOREIGN KEY ("user_role_id") REFERENCES "user_roles" ("user_role_id");

ALTER TABLE "users" ADD FOREIGN KEY ("channel_id") REFERENCES "channels" ("channel_id");

ALTER TABLE "product_main" ADD FOREIGN KEY ("product_category_id") REFERENCES "product_categories" ("product_category_id");

ALTER TABLE "product_main" ADD FOREIGN KEY ("create_by") REFERENCES "users" ("user_id");

ALTER TABLE "product_main" ADD FOREIGN KEY ("update_by") REFERENCES "users" ("user_id");

ALTER TABLE "product_suppliers" ADD FOREIGN KEY ("supplier_id") REFERENCES "suppliers" ("supplier_id");

ALTER TABLE "product_suppliers" ADD FOREIGN KEY ("product_main_id") REFERENCES "product_main" ("product_main_id");

ALTER TABLE "product_suppliers" ADD FOREIGN KEY ("create_by") REFERENCES "users" ("user_id");

ALTER TABLE "product_suppliers" ADD FOREIGN KEY ("update_by") REFERENCES "users" ("user_id");

ALTER TABLE "product_channels" ADD FOREIGN KEY ("channel_id") REFERENCES "channels" ("channel_id");

ALTER TABLE "product_channels" ADD FOREIGN KEY ("product_main_id") REFERENCES "product_main" ("product_main_id");

ALTER TABLE "product_channels" ADD FOREIGN KEY ("create_by") REFERENCES "users" ("user_id");

ALTER TABLE "product_channels" ADD FOREIGN KEY ("update_by") REFERENCES "users" ("user_id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("channel_id") REFERENCES "channels" ("channel_id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("product_channels_id") REFERENCES "product_channels" ("product_channels_id");

ALTER TABLE "transactions" ADD FOREIGN KEY ("create_by") REFERENCES "users" ("user_id");

ALTER TABLE "transaction_routes" ADD FOREIGN KEY ("transaction_id") REFERENCES "transactions" ("transaction_id");

ALTER TABLE "transaction_routes" ADD FOREIGN KEY ("transaction_route_id") REFERENCES "transactions" ("current_route_id");

ALTER TABLE "rc_suppliers" ADD FOREIGN KEY ("supplier_id") REFERENCES "suppliers" ("supplier_id");

ALTER TABLE "rc_mappers" ADD FOREIGN KEY ("rc_main_id") REFERENCES "rc_main" ("rc_main_id");

ALTER TABLE "rc_mappers" ADD FOREIGN KEY ("rc_supplier_id") REFERENCES "rc_suppliers" ("rc_supplier_id");
