import { Module } from '@nestjs/common';
import { RouterModule } from 'nest-router';
import { ProductModule } from './common/product/product.module';
import { SinglePaymentModule } from './common/single-payment/single-payment.module';
import { InquiryPostpaidModule } from './common/inquiry-postpaid/inquiry-postpaid.module';
import { PaymentPostpaidModule } from './common/payment-postpaid/payment-postpaid.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import routes from './routes';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmConfigService } from './configs/database';
import { AuthenticationModule } from './shared/common/authentication/authentication.module';

@Module({
    imports: [
        ConfigModule.forRoot({ isGlobal: true }), /* Load .env */
        RouterModule.forRoutes(routes), /* Load routes */
        TypeOrmModule.forRootAsync({ useClass: TypeOrmConfigService }), /* Connect To Database */
        ProductModule,
        SinglePaymentModule,
        InquiryPostpaidModule,
        PaymentPostpaidModule,
        AuthenticationModule
    ],
    controllers: [],
    providers: [],
})
export class AppModule {}
