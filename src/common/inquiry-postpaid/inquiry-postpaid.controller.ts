import { Controller } from '@nestjs/common';
import { InquiryPostpaidService } from './inquiry-postpaid.service';

@Controller()
export class InquiryPostpaidController {
    constructor(private readonly inquiryPostpaidService: InquiryPostpaidService) {}
}
