import { Module } from '@nestjs/common';
import { InquiryPostpaidService } from './inquiry-postpaid.service';
import { InquiryPostpaidController } from './inquiry-postpaid.controller';

@Module({
  controllers: [InquiryPostpaidController],
  providers: [InquiryPostpaidService]
})
export class InquiryPostpaidModule {}
