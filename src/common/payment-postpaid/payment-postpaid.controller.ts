import { Controller } from '@nestjs/common';
import { PaymentPostpaidService } from './payment-postpaid.service';

@Controller()
export class PaymentPostpaidController {
    constructor(private readonly paymentPostpaidService: PaymentPostpaidService) {}
}
