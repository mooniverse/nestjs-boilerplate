import { Module } from '@nestjs/common';
import { PaymentPostpaidService } from './payment-postpaid.service';
import { PaymentPostpaidController } from './payment-postpaid.controller';

@Module({
  controllers: [PaymentPostpaidController],
  providers: [PaymentPostpaidService]
})
export class PaymentPostpaidModule {}
