import { Body, Controller, Get, HttpException, HttpStatus, Param, Query, Req, Res, UseGuards, UseInterceptors } from '@nestjs/common';
import { query, Request, Response } from 'express';
import { JwtAuthGuard } from 'src/shared/guards/jwt.guard';
import { RoleGuard, Roles } from 'src/shared/guards/role.guard';
import { ControllerResponseDto } from 'src/shared/dto/response.dto';
import { RC, USER_ROLE } from 'src/shared/enum';
import { IdProductDto, SearchProductDto } from './product.dto';
import { ProductService } from './product.service';
import { LANG_EN, LANG_ID } from 'src/shared/enum/languages';

@Controller()
@UseGuards(JwtAuthGuard, RoleGuard)
export class ProductController {
    
    constructor(private readonly productService: ProductService) { }
    
    @Get("/main")
    @Roles(USER_ROLE.SUPER_ADMIN)
    public async getAllMainProduct(@Query() query: SearchProductDto): Promise<ControllerResponseDto> {

        const resultMainProduct = await this.productService.findMainProduct(query.limit, query.offset, query.search);
        const totalMainProduct = await this.productService.countMainProduct(query.search);
        
        return {
            httpStatus: HttpStatus.OK,
            rc: RC.OK,
            msgID: LANG_ID.SUCCESS,
            msgEN: LANG_EN.SUCCESS,
            data: {
                count: totalMainProduct,
                rows: resultMainProduct
            }
        }

    }

    @Get("/main/:id")
    @Roles(USER_ROLE.SUPER_ADMIN)
    public async getMainProductDetail(@Param() param: IdProductDto): Promise<ControllerResponseDto> {

        const resultMainProduct = await this.productService.findMainProductById(param.id);
        if (!resultMainProduct) {
            return {
                httpStatus: HttpStatus.NOT_FOUND,
                rc: RC.NOT_FOUND,
                msgID: LANG_ID.PRODUCT_NOT_FOUND,
                msgEN: LANG_EN.PRODUCT_NOT_FOUND,
                data: {}
            }
        }

        return {
            httpStatus: HttpStatus.OK,
            rc: RC.OK,
            msgID: LANG_ID.SUCCES_GET_MAIN_PRODUCT,
            msgEN: LANG_EN.SUCCES_GET_MAIN_PRODUCT,
            data: resultMainProduct
        }

    }

    @Get("/channel")
    @Roles(USER_ROLE.SUPER_ADMIN)
    public async getAllChannelProduct(@Query() query: SearchProductDto): Promise<ControllerResponseDto> {

        const resultChannelProduct = await this.productService.findChannelProduct(query.limit, query.offset, query.search);
        const totalChannelProduct = await this.productService.countChannelProduct(query.search)

        return {
            httpStatus: HttpStatus.OK,
            rc: RC.OK,
            msgID: LANG_ID.SUCCESS_GET_CHANNEL_PRODUCT,
            msgEN: LANG_EN.SUCCESS_GET_CHANNEL_PRODUCT,
            data: {
                count: totalChannelProduct,
                rows: resultChannelProduct
            }
        }

    }

    @Get("/channel/:id")
    @Roles(USER_ROLE.SUPER_ADMIN)
    public async getChannelProductDetail(@Param() param: IdProductDto): Promise<ControllerResponseDto> {

        const resultChannelProduct = await this.productService.findChannelProductById(param.id);
        if (!resultChannelProduct) {
            return {
                httpStatus: HttpStatus.NOT_FOUND,
                rc: RC.NOT_FOUND,
                msgID: LANG_ID.PRODUCT_NOT_FOUND,
                msgEN: LANG_EN.PRODUCT_NOT_FOUND,
                data: {}
            }
        }

        return {
            httpStatus: HttpStatus.OK,
            rc: RC.OK,
            msgID: LANG_ID.SUCCESS_GET_CHANNEL_PRODUCT,
            msgEN: LANG_EN.SUCCESS_GET_CHANNEL_PRODUCT,
            data: resultChannelProduct
        }

    }

}
