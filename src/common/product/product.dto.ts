import { IsOptional, IsNumber, IsNotEmpty, IsInt, Min, Max, IsString, Length, IsAlphanumeric } from 'class-validator';

export class SearchProductDto {

    @IsOptional()
    @IsInt()
    @Min(0)
    @Max(1000)
    public limit: number = 0;

    @IsOptional()
    @IsInt()
    @Min(0)
    @Max(1000)
    public offset: number = 0;

    @IsOptional()
    @IsString()
    @Length(0, 100)
    public search: string = '';

}

export class IdProductDto {
    
    @IsAlphanumeric()
    @Length(0, 25)
    public id:string;
    
}