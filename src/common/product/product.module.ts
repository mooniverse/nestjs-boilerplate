import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Entities from 'src/entities/index';

@Module({
    imports: [TypeOrmModule.forFeature([...Object.values(Entities)])],
    controllers: [ProductController],
    providers: [ProductService],
})
export class ProductModule { }
