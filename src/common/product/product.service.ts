import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ProductMain, ProductChannels } from 'src/entities';
import { Raw, Repository } from 'typeorm';

@Injectable()
export class ProductService {
    constructor(
        @InjectRepository(ProductMain)
        private productMain: Repository<ProductMain>,
        @InjectRepository(ProductChannels)
        private productChannels: Repository<ProductChannels>,
    ) { }

    public async findMainProduct(limit: number, offset: number, search: string): Promise<ProductMain[]> {
        return this.productMain.find({
            where: [
                { productMainId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) },
                { productName: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) }
            ],
            take: limit,
            skip: offset,
            order: {
                productName: "ASC"
            },
        });
    }

    public async findMainProductById(id: string): Promise<ProductMain> {
        return this.productMain.findOneBy({
            productMainId: id
        })
    }

    public async countMainProduct(search: string) {
        return this.productMain.count({
            where: [
                { productMainId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) },
                { productName: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) }
            ],
        });
    }

    public async findChannelProduct(limit: number, offset: number, search: string): Promise<ProductChannels[]> {
        return this.productChannels.find({
            where: [
                { channelId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) },
                { customProductId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) }
            ],
            take: limit,
            skip: offset,
            order: {
                channelId: "ASC"
            },
        })
    }

    public async findChannelProductById(id: string): Promise<ProductChannels> {
        return this.productChannels.findOneBy({
            productChannelsId: id
        })
    }

    public async countChannelProduct(search: string) {
        return this.productChannels.count({
            where: [
                { channelId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) },
                { customProductId: Raw((alias) => `LOWER(${alias}) LIKE '%${search.toLowerCase()}%'`) }
            ],
        })
    }

}
