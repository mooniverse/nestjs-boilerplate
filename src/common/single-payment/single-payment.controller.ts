import { Controller } from '@nestjs/common';
import { SinglePaymentService } from './single-payment.service';

@Controller()
export class SinglePaymentController {
    constructor(private readonly singlePaymentService: SinglePaymentService) {}
}
