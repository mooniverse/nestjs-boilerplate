import { Module } from '@nestjs/common';
import { SinglePaymentService } from './single-payment.service';
import { SinglePaymentController } from './single-payment.controller';

@Module({
  controllers: [SinglePaymentController],
  providers: [SinglePaymentService]
})
export class SinglePaymentModule {}
