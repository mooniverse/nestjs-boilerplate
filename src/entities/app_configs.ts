import { Column, Entity, Index } from "typeorm";

@Index("app_configs_pkey", ["key"], { unique: true })
@Entity("app_configs", { schema: "public" })
export class AppConfigs {
  @Column("character varying", { primary: true, name: "key", length: 75 })
  key: string;

  @Column("character varying", { name: "value", nullable: true, length: 500 })
  value: string | null;
}
