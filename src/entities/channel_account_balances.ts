import {
  Column,
  Entity,
  Index,
  JoinColumn,
  OneToMany,
  OneToOne,
} from "typeorm";
import { Channels } from "./channels";
import { ChannelAccountVas } from "./channel_account_vas";

@Index("channel_account_balances_account_number_key", ["accountNumber"], {
  unique: true,
})
@Index("channel_account_balances_pkey", ["channelAccountBalanceId"], {
  unique: true,
})
@Index("channel_account_balances_channel_id_key", ["channelId"], {
  unique: true,
})
@Entity("channel_account_balances", { schema: "public" })
export class ChannelAccountBalances {
  @Column("character varying", {
    primary: true,
    name: "channel_account_balance_id",
    length: 25,
  })
  channelAccountBalanceId: string;

  @Column("character varying", { name: "channel_id", unique: true, length: 25 })
  channelId: string;

  @Column("character varying", {
    name: "account_number",
    unique: true,
    length: 30,
  })
  accountNumber: string;

  @OneToOne(() => Channels, (channels) => channels.channelAccountBalances)
  @JoinColumn([{ name: "channel_id", referencedColumnName: "channelId" }])
  channel: Channels;

  @OneToMany(
    () => ChannelAccountVas,
    (channelAccountVas) => channelAccountVas.channelAccountBalance
  )
  channelAccountVas: ChannelAccountVas[];
}
