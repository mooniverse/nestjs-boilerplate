import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { ChannelAccountBalances } from "./channel_account_balances";

@Index("channel_account_vas_pkey", ["channelAccountVaId"], { unique: true })
@Entity("channel_account_vas", { schema: "public" })
export class ChannelAccountVas {
  @Column("character varying", {
    primary: true,
    name: "channel_account_va_id",
    length: 25,
  })
  channelAccountVaId: string;

  @Column("character varying", { name: "vendor", nullable: true, length: 75 })
  vendor: string | null;

  @Column("character varying", { name: "vendor_ref_id", length: 50 })
  vendorRefId: string;

  @Column("boolean", { name: "is_active" })
  isActive: boolean;

  @Column("character varying", { name: "va_number", length: 75 })
  vaNumber: string;

  @Column("character varying", { name: "va_type", length: 50 })
  vaType: string;

  @Column("character varying", { name: "va_status", length: 50 })
  vaStatus: string;

  @Column("bigint", { name: "amount", nullable: true, default: () => "0" })
  amount: string | null;

  @Column("timestamp without time zone", {
    name: "create_time",
    default: () => "now()",
  })
  createTime: Date;

  @Column("character varying", {
    name: "create_by",
    nullable: true,
    length: 25,
  })
  createBy: string | null;

  @Column("timestamp without time zone", { name: "paid_time", nullable: true })
  paidTime: Date | null;

  @Column("timestamp without time zone", {
    name: "expire_time",
    nullable: true,
  })
  expireTime: Date | null;

  @Column("jsonb", { name: "data", nullable: true, default: {} })
  data: object | null;

  @ManyToOne(
    () => ChannelAccountBalances,
    (channelAccountBalances) => channelAccountBalances.channelAccountVas
  )
  @JoinColumn([
    {
      name: "channel_account_balance_id",
      referencedColumnName: "channelAccountBalanceId",
    },
  ])
  channelAccountBalance: ChannelAccountBalances;
}
