import { Column, Entity, Index, OneToMany, OneToOne } from "typeorm";
import { ChannelAccountBalances } from "./channel_account_balances";
import { ProductChannels } from "./product_channels";
import { Transactions } from "./transactions";
import { Users } from "./users";

@Index("channels_pkey", ["channelId"], { unique: true })
@Index("channels_cif_number_key", ["cifNumber"], { unique: true })
@Entity("channels", { schema: "public" })
export class Channels {
  @Column("character varying", {
    primary: true,
    name: "channel_id",
    length: 25,
  })
  channelId: string;

  @Column("character varying", { name: "channel_name", length: 255 })
  channelName: string;

  @Column("boolean", { name: "is_active" })
  isActive: boolean;

  @Column("character varying", { name: "cif_number", unique: true, length: 30 })
  cifNumber: string;

  @Column("character varying", { name: "callback_url", length: 255 })
  callbackUrl: string;

  @Column("timestamp without time zone", {
    name: "create_time",
    default: () => "now()",
  })
  createTime: Date;

  @OneToOne(
    () => ChannelAccountBalances,
    (channelAccountBalances) => channelAccountBalances.channel
  )
  channelAccountBalances: ChannelAccountBalances;

  @OneToMany(
    () => ProductChannels,
    (productChannels) => productChannels.channel
  )
  productChannels: ProductChannels[];

  @OneToMany(() => Transactions, (transactions) => transactions.channel)
  transactions: Transactions[];

  @OneToMany(() => Users, (users) => users.channel)
  users: Users[];
}
