import { Column, Entity, Index, OneToMany } from "typeorm";
import { ProductMain } from "./product_main";

@Index("product_categories_category_alias_key", ["categoryAlias"], {
  unique: true,
})
@Index("product_categories_category_alias_idx", ["categoryAlias"], {})
@Index("product_categories_pkey", ["productCategoryId"], { unique: true })
@Entity("product_categories", { schema: "public" })
export class ProductCategories {
  @Column("character varying", {
    primary: true,
    name: "product_category_id",
    length: 25,
  })
  productCategoryId: string;

  @Column("character varying", {
    name: "category_alias",
    unique: true,
    length: 75,
  })
  categoryAlias: string;

  @Column("character varying", { name: "category_name", length: 150 })
  categoryName: string;

  @Column("timestamp without time zone", {
    name: "create_time",
    default: () => "now()",
  })
  createTime: Date;

  @OneToMany(() => ProductMain, (productMain) => productMain.productCategory)
  productMains: ProductMain[];
}
