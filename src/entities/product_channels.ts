import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { Channels } from "./channels";
import { Users } from "./users";
import { ProductMain } from "./product_main";
import { Transactions } from "./transactions";

@Index(
  "product_channels_custom_product_id_channel_id_idx",
  ["channelId", "customProductId"],
  { unique: true }
)
@Index("product_channels_channel_id_idx", ["channelId"], {})
@Index("product_channels_is_active_idx", ["isActive"], {})
@Index("product_channels_pkey", ["productChannelsId"], { unique: true })
@Entity("product_channels", { schema: "public" })
export class ProductChannels {
  @Column("character varying", {
    primary: true,
    name: "product_channels_id",
    length: 25,
  })
  productChannelsId: string;

  @Column("character varying", {
    name: "custom_product_id",
    nullable: true,
    length: 100,
  })
  customProductId: string | null;

  @Column("character varying", { name: "channel_id", length: 25 })
  channelId: string;

  @Column("boolean", {
    name: "is_active",
    nullable: true,
    default: () => "true",
  })
  isActive: boolean | null;

  @Column("timestamp without time zone", {
    name: "create_time",
    nullable: true,
    default: () => "now()",
  })
  createTime: Date | null;

  @Column("timestamp without time zone", {
    name: "update_time",
    nullable: true,
    default: () => "now()",
  })
  updateTime: Date | null;

  @Column("jsonb", {
    name: "price_config",
    nullable: true,
    default: [
      {
        key: "selling_price",
        name: "Sell Price",
        value: "0",
        show_to_receipt: true,
        calculation_type: "BASED_PRICE",
        mandatory_fields: true,
        adjust_to_channel: true,
        adjust_to_customer: true,
      },
      {
        key: "admin",
        name: "Admin",
        value: "0",
        show_to_receipt: true,
        calculation_type: "BASED_PRICE",
        mandatory_fields: true,
        adjust_to_channel: true,
        adjust_to_customer: true,
      },
      {
        key: "customadmin",
        name: "Custom Admin",
        value: "2500",
        show_to_receipt: true,
        calculation_type: "FIXED_PRICE",
        mandatory_fields: false,
        adjust_to_channel: false,
        adjust_to_customer: true,
      },
    ],
  })
  priceConfig: object | null;

  @ManyToOne(() => Channels, (channels) => channels.productChannels)
  @JoinColumn([{ name: "channel_id", referencedColumnName: "channelId" }])
  channel: Channels;

  @ManyToOne(() => Users, (users) => users.productChannels)
  @JoinColumn([{ name: "create_by", referencedColumnName: "userId" }])
  createBy: Users;

  @ManyToOne(() => ProductMain, (productMain) => productMain.productChannels)
  @JoinColumn([
    { name: "product_main_id", referencedColumnName: "productMainId" },
  ])
  productMain: ProductMain;

  @ManyToOne(() => Users, (users) => users.productChannels2)
  @JoinColumn([{ name: "update_by", referencedColumnName: "userId" }])
  updateBy: Users;

  @OneToMany(() => Transactions, (transactions) => transactions.productChannels)
  transactions: Transactions[];
}
