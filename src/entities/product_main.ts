import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { ProductChannels } from "./product_channels";
import { Users } from "./users";
import { ProductCategories } from "./product_categories";
import { ProductSuppliers } from "./product_suppliers";

@Index("product_main_is_single_payment_idx", ["isSinglePayment"], {})
@Index("product_main_pkey", ["productMainId"], { unique: true })
@Index("product_main_product_name_idx", ["productName"], {})
@Entity("product_main", { schema: "public" })
export class ProductMain {
  @Column("character varying", {
    primary: true,
    name: "product_main_id",
    length: 25,
  })
  productMainId: string;

  @Column("character varying", { name: "product_name", length: 255 })
  productName: string;

  @Column("boolean", { name: "is_active" })
  isActive: boolean;

  @Column("boolean", { name: "is_single_payment" })
  isSinglePayment: boolean;

  @Column("jsonb", { name: "mandatory_fields", nullable: true, default: {} })
  mandatoryFields: object | null;

  @Column("character varying", { name: "description", length: 255 })
  description: string;

  @Column("timestamp without time zone", {
    name: "create_time",
    nullable: true,
    default: () => "now()",
  })
  createTime: Date | null;

  @Column("timestamp without time zone", {
    name: "update_time",
    nullable: true,
    default: () => "now()",
  })
  updateTime: Date | null;

  @OneToMany(
    () => ProductChannels,
    (productChannels) => productChannels.productMain
  )
  productChannels: ProductChannels[];

  @ManyToOne(() => Users, (users) => users.productMains)
  @JoinColumn([{ name: "create_by", referencedColumnName: "userId" }])
  createBy: Users;

  @ManyToOne(
    () => ProductCategories,
    (productCategories) => productCategories.productMains
  )
  @JoinColumn([
    { name: "product_category_id", referencedColumnName: "productCategoryId" },
  ])
  productCategory: ProductCategories;

  @ManyToOne(() => Users, (users) => users.productMains2)
  @JoinColumn([{ name: "update_by", referencedColumnName: "userId" }])
  updateBy: Users;

  @OneToMany(
    () => ProductSuppliers,
    (productSuppliers) => productSuppliers.productMain
  )
  productSuppliers: ProductSuppliers[];
}
