import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { Users } from "./users";
import { ProductMain } from "./product_main";
import { Suppliers } from "./suppliers";

@Index("product_suppliers_is_active_idx", ["isActive"], {})
@Index("product_suppliers_pkey", ["productSupplierId"], { unique: true })
@Index("product_suppliers_supplier_id_idx", ["supplierId"], {})
@Entity("product_suppliers", { schema: "public" })
export class ProductSuppliers {
  @Column("character varying", {
    primary: true,
    name: "product_supplier_id",
    length: 25,
  })
  productSupplierId: string;

  @Column("character varying", { name: "supplier_id", length: 25 })
  supplierId: string;

  @Column("character varying", { name: "product_supplier_ref_id", length: 255 })
  productSupplierRefId: string;

  @Column("character varying", { name: "product_supplier_name", length: 255 })
  productSupplierName: string;

  @Column("boolean", {
    name: "is_active",
    nullable: true,
    default: () => "true",
  })
  isActive: boolean | null;

  @Column("integer", { name: "buying_price", default: () => "0" })
  buyingPrice: number;

  @Column("integer", { name: "admin_price", default: () => "0" })
  adminPrice: number;

  @Column("integer", { name: "admin_commission", default: () => "0" })
  adminCommission: number;

  @Column("integer", { name: "margin_of_admin", default: () => "0" })
  marginOfAdmin: number;

  @Column("character varying", {
    name: "description",
    nullable: true,
    length: 255,
  })
  description: string | null;

  @Column("timestamp without time zone", {
    name: "create_time",
    nullable: true,
    default: () => "now()",
  })
  createTime: Date | null;

  @Column("timestamp without time zone", {
    name: "update_time",
    nullable: true,
    default: () => "now()",
  })
  updateTime: Date | null;

  @ManyToOne(() => Users, (users) => users.productSuppliers)
  @JoinColumn([{ name: "create_by", referencedColumnName: "userId" }])
  createBy: Users;

  @ManyToOne(() => ProductMain, (productMain) => productMain.productSuppliers)
  @JoinColumn([
    { name: "product_main_id", referencedColumnName: "productMainId" },
  ])
  productMain: ProductMain;

  @ManyToOne(() => Suppliers, (suppliers) => suppliers.productSuppliers)
  @JoinColumn([{ name: "supplier_id", referencedColumnName: "supplierId" }])
  supplier: Suppliers;

  @ManyToOne(() => Users, (users) => users.productSuppliers2)
  @JoinColumn([{ name: "update_by", referencedColumnName: "userId" }])
  updateBy: Users;
}
