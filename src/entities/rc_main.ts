import { Column, Entity, Index, OneToMany } from "typeorm";
import { RcMappers } from "./rc_mappers";

@Index("rc_main_rc_key", ["rc"], { unique: true })
@Index("rc_main_pkey", ["rcMainId"], { unique: true })
@Entity("rc_main", { schema: "public" })
export class RcMain {
  @Column("character varying", {
    primary: true,
    name: "rc_main_id",
    length: 25,
  })
  rcMainId: string;

  @Column("character varying", {
    name: "rc",
    nullable: true,
    unique: true,
    length: 20,
  })
  rc: string | null;

  @Column("character varying", { name: "message", nullable: true, length: 255 })
  message: string | null;

  @OneToMany(() => RcMappers, (rcMappers) => rcMappers.rcMain)
  rcMappers: RcMappers[];
}
