import { Column, Entity, Index, JoinColumn, ManyToOne } from "typeorm";
import { RcMain } from "./rc_main";
import { RcSuppliers } from "./rc_suppliers";

@Index("rc_mappers_pkey", ["rcMapperId"], { unique: true })
@Entity("rc_mappers", { schema: "public" })
export class RcMappers {
  @Column("character varying", {
    primary: true,
    name: "rc_mapper_id",
    length: 25,
  })
  rcMapperId: string;

  @ManyToOne(() => RcMain, (rcMain) => rcMain.rcMappers)
  @JoinColumn([{ name: "rc_main_id", referencedColumnName: "rcMainId" }])
  rcMain: RcMain;

  @ManyToOne(() => RcSuppliers, (rcSuppliers) => rcSuppliers.rcMappers)
  @JoinColumn([
    { name: "rc_supplier_id", referencedColumnName: "rcSupplierId" },
  ])
  rcSupplier: RcSuppliers;
}
