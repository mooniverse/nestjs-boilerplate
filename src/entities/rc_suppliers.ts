import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { RcMappers } from "./rc_mappers";
import { Suppliers } from "./suppliers";

@Index("rc_suppliers_pkey", ["rcSupplierId"], { unique: true })
@Entity("rc_suppliers", { schema: "public" })
export class RcSuppliers {
  @Column("character varying", {
    primary: true,
    name: "rc_supplier_id",
    length: 25,
  })
  rcSupplierId: string;

  @Column("character varying", {
    name: "rc_supplier",
    nullable: true,
    length: 20,
  })
  rcSupplier: string | null;

  @Column("character varying", { name: "message", nullable: true, length: 255 })
  message: string | null;

  @Column("boolean", { name: "reroute", nullable: true })
  reroute: boolean | null;

  @OneToMany(() => RcMappers, (rcMappers) => rcMappers.rcSupplier)
  rcMappers: RcMappers[];

  @ManyToOne(() => Suppliers, (suppliers) => suppliers.rcSuppliers)
  @JoinColumn([{ name: "supplier_id", referencedColumnName: "supplierId" }])
  supplier: Suppliers;
}
