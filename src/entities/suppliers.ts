import { Column, Entity, Index, OneToMany } from "typeorm";
import { ProductSuppliers } from "./product_suppliers";
import { RcSuppliers } from "./rc_suppliers";

@Index("suppliers_supplier_alias_key", ["supplierAlias"], { unique: true })
@Index("suppliers_pkey", ["supplierId"], { unique: true })
@Entity("suppliers", { schema: "public" })
export class Suppliers {
  @Column("character varying", {
    primary: true,
    name: "supplier_id",
    length: 25,
  })
  supplierId: string;

  @Column("character varying", {
    name: "supplier_alias",
    unique: true,
    length: 50,
  })
  supplierAlias: string;

  @Column("character varying", {
    name: "supplier_name",
    nullable: true,
    length: 255,
  })
  supplierName: string | null;

  @Column("boolean", { name: "is_active" })
  isActive: boolean;

  @Column("timestamp without time zone", {
    name: "create_time",
    default: () => "now()",
  })
  createTime: Date;

  @Column("jsonb", { name: "additional_data", nullable: true, default: {} })
  additionalData: object | null;

  @OneToMany(
    () => ProductSuppliers,
    (productSuppliers) => productSuppliers.supplier
  )
  productSuppliers: ProductSuppliers[];

  @OneToMany(() => RcSuppliers, (rcSuppliers) => rcSuppliers.supplier)
  rcSuppliers: RcSuppliers[];
}
