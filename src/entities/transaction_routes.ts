import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToOne,
} from "typeorm";
import { Transactions } from "./transactions";

@Index("transaction_routes_is_success_idx", ["isSuccess"], {})
@Index(
  "transaction_routes_transaction_id_route_number_idx",
  ["routeNumber", "transactionId"],
  { unique: true }
)
@Index(
  "transaction_routes_supplier_id_supplier_reference_idx",
  ["supplierId", "supplierReference"],
  { unique: true }
)
@Index(
  "transaction_routes_transaction_id_supplier_id_idx",
  ["supplierId", "transactionId"],
  { unique: true }
)
@Index("transaction_routes_transaction_id_idx", ["transactionId"], {})
@Index("transaction_routes_pkey", ["transactionRouteId"], { unique: true })
@Entity("transaction_routes", { schema: "public" })
export class TransactionRoutes {
  @Column("character varying", {
    primary: true,
    name: "transaction_route_id",
    length: 25,
  })
  transactionRouteId: string;

  @Column("character varying", { name: "transaction_id", length: 25 })
  transactionId: string;

  @Column("integer", { name: "route_number", default: () => "1" })
  routeNumber: number;

  @Column("character varying", { name: "supplier_id", length: 25 })
  supplierId: string;

  @Column("character varying", {
    name: "supplier_reference",
    nullable: true,
    length: 100,
  })
  supplierReference: string | null;

  @Column("enum", {
    name: "transaction_route_status",
    nullable: true,
    enum: [
      "CREATED",
      "INQUIRY",
      "ON_ROUTING",
      "ON_PROCESS",
      "FAILED",
      "SUCCESS",
    ],
  })
  transactionRouteStatus:
    | "CREATED"
    | "INQUIRY"
    | "ON_ROUTING"
    | "ON_PROCESS"
    | "FAILED"
    | "SUCCESS"
    | null;

  @Column("timestamp without time zone", {
    name: "create_time",
    nullable: true,
    default: () => "now()",
  })
  createTime: Date | null;

  @Column("jsonb", { name: "data", nullable: true, default: {} })
  data: object | null;

  @Column("jsonb", { name: "callback_supplier", nullable: true, default: {} })
  callbackSupplier: object | null;

  @Column("boolean", {
    name: "is_success",
    nullable: true,
    default: () => "false",
  })
  isSuccess: boolean | null;

  @ManyToOne(
    () => Transactions,
    (transactions) => transactions.transactionRoutes
  )
  @JoinColumn([
    { name: "transaction_id", referencedColumnName: "transactionId" },
  ])
  transaction: Transactions;

  @OneToOne(
    () => Transactions,
    (transactions) => transactions.transactionRoutes2
  )
  @JoinColumn([
    { name: "transaction_route_id", referencedColumnName: "currentRouteId" },
  ])
  transactionRoute: Transactions;
}
