import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
} from "typeorm";
import { TransactionRoutes } from "./transaction_routes";
import { Channels } from "./channels";
import { Users } from "./users";
import { ProductChannels } from "./product_channels";

@Index("transactions_channel_id_idx", ["channelId"], {})
@Index(
  "transactions_channel_id_channel_reference_idx",
  ["channelId", "channelReference"],
  { unique: true }
)
@Index("transactions_channel_reference_idx", ["channelReference"], {})
@Index("transactions_current_route_id_key", ["currentRouteId"], {
  unique: true,
})
@Index("transactions_product_channels_id_idx", ["productChannelsId"], {})
@Index("transactions_pkey", ["transactionId"], { unique: true })
@Index("transactions_transaction_status_idx", ["transactionStatus"], {})
@Entity("transactions", { schema: "public" })
export class Transactions {
  @Column("character varying", {
    primary: true,
    name: "transaction_id",
    length: 25,
  })
  transactionId: string;

  @Column("character varying", { name: "channel_id", length: 25 })
  channelId: string;

  @Column("character varying", { name: "channel_reference", length: 255 })
  channelReference: string;

  @Column("character varying", { name: "product_channels_id", length: 25 })
  productChannelsId: string;

  @Column("integer", { name: "buying_price" })
  buyingPrice: number;

  @Column("integer", { name: "price_channel" })
  priceChannel: number;

  @Column("integer", { name: "price_customer" })
  priceCustomer: number;

  @Column("integer", { name: "net_margin_platform" })
  netMarginPlatform: number;

  @Column("enum", {
    name: "transaction_status",
    enum: [
      "CREATED",
      "INQUIRY",
      "ON_ROUTING",
      "ON_PROCESS",
      "FAILED",
      "SUCCESS",
    ],
  })
  transactionStatus:
    | "CREATED"
    | "INQUIRY"
    | "ON_ROUTING"
    | "ON_PROCESS"
    | "FAILED"
    | "SUCCESS";

  @Column("timestamp without time zone", {
    name: "create_time",
    nullable: true,
    default: () => "now()",
  })
  createTime: Date | null;

  @Column("jsonb", { name: "customer_data", nullable: true, default: {} })
  customerData: object | null;

  @Column("character varying", {
    name: "payment_reference",
    nullable: true,
    length: 50,
  })
  paymentReference: string | null;

  @Column("boolean", { name: "is_reversal", default: () => "false" })
  isReversal: boolean;

  @Column("jsonb", { name: "callback_channel", nullable: true, default: {} })
  callbackChannel: object | null;

  @Column("jsonb", { name: "receipt_customer", nullable: true, default: {} })
  receiptCustomer: object | null;

  @Column("character varying", {
    name: "current_route_id",
    nullable: true,
    unique: true,
    length: 25,
  })
  currentRouteId: string | null;

  @OneToMany(
    () => TransactionRoutes,
    (transactionRoutes) => transactionRoutes.transaction
  )
  transactionRoutes: TransactionRoutes[];

  @OneToOne(
    () => TransactionRoutes,
    (transactionRoutes) => transactionRoutes.transactionRoute
  )
  transactionRoutes2: TransactionRoutes;

  @ManyToOne(() => Channels, (channels) => channels.transactions)
  @JoinColumn([{ name: "channel_id", referencedColumnName: "channelId" }])
  channel: Channels;

  @ManyToOne(() => Users, (users) => users.transactions)
  @JoinColumn([{ name: "create_by", referencedColumnName: "userId" }])
  createBy: Users;

  @ManyToOne(
    () => ProductChannels,
    (productChannels) => productChannels.transactions
  )
  @JoinColumn([
    { name: "product_channels_id", referencedColumnName: "productChannelsId" },
  ])
  productChannels: ProductChannels;
}
