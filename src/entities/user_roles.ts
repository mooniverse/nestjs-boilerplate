import { Column, Entity, Index, OneToMany } from "typeorm";
import { Users } from "./users";

@Index("user_roles_pkey", ["userRoleId"], { unique: true })
@Entity("user_roles", { schema: "public" })
export class UserRoles {
  @Column("character varying", {
    primary: true,
    name: "user_role_id",
    length: 25,
  })
  userRoleId: string;

  @Column("character varying", { name: "role", nullable: true, length: 100 })
  role: string | null;

  @OneToMany(() => Users, (users) => users.userRole)
  users: Users[];
}
