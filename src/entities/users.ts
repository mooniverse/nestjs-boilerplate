import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
} from "typeorm";
import { ProductChannels } from "./product_channels";
import { ProductMain } from "./product_main";
import { ProductSuppliers } from "./product_suppliers";
import { Transactions } from "./transactions";
import { Channels } from "./channels";
import { UserRoles } from "./user_roles";

@Index("users_user_fullname_idx", ["userFullname"], {})
@Index("users_pkey", ["userId"], { unique: true })
@Index("users_username_key", ["username"], { unique: true })
@Index("users_username_idx", ["username"], { unique: true })
@Entity("users", { schema: "public" })
export class Users {
  @Column("character varying", { primary: true, name: "user_id", length: 25 })
  userId: string;

  @Column("character varying", { name: "username", unique: true, length: 100 })
  username: string;

  @Column("character varying", { name: "password", length: 255 })
  password: string;

  @Column("character varying", { name: "api_key", nullable: true, length: 255 })
  apiKey: string | null;

  @Column("character varying", {
    name: "api_secret",
    nullable: true,
    length: 255,
  })
  apiSecret: string | null;

  @Column("character varying", {
    name: "user_fullname",
    nullable: true,
    length: 255,
  })
  userFullname: string | null;

  @Column("timestamp without time zone", {
    name: "create_time",
    default: () => "now()",
  })
  createTime: Date;

  @Column("character varying", {
    name: "create_by",
    nullable: true,
    length: 25,
  })
  createBy: string | null;

  @Column("timestamp without time zone", {
    name: "update_time",
    default: () => "now()",
  })
  updateTime: Date;

  @Column("character varying", {
    name: "update_by",
    nullable: true,
    length: 25,
  })
  updateBy: string | null;

  @OneToMany(
    () => ProductChannels,
    (productChannels) => productChannels.createBy
  )
  productChannels: ProductChannels[];

  @OneToMany(
    () => ProductChannels,
    (productChannels) => productChannels.updateBy
  )
  productChannels2: ProductChannels[];

  @OneToMany(() => ProductMain, (productMain) => productMain.createBy)
  productMains: ProductMain[];

  @OneToMany(() => ProductMain, (productMain) => productMain.updateBy)
  productMains2: ProductMain[];

  @OneToMany(
    () => ProductSuppliers,
    (productSuppliers) => productSuppliers.createBy
  )
  productSuppliers: ProductSuppliers[];

  @OneToMany(
    () => ProductSuppliers,
    (productSuppliers) => productSuppliers.updateBy
  )
  productSuppliers2: ProductSuppliers[];

  @OneToMany(() => Transactions, (transactions) => transactions.createBy)
  transactions: Transactions[];

  @ManyToOne(() => Channels, (channels) => channels.users)
  @JoinColumn([{ name: "channel_id", referencedColumnName: "channelId" }])
  channel: Channels;

  @ManyToOne(() => UserRoles, (userRoles) => userRoles.users)
  @JoinColumn([{ name: "user_role_id", referencedColumnName: "userRoleId" }])
  userRole: UserRoles;
}
