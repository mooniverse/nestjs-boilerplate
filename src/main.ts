import {
    BadRequestException,
    HttpStatus,
    ValidationError,
    ValidationPipe,
} from "@nestjs/common";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { ResponseInterceptor } from "./shared/interceptors/response.interceptor";
import { ResponseDto } from "./shared/dto/response.dto";
import { RC } from "./shared/enum";
import { HttpExceptionFilter } from "./shared/filters/exception.filter";

async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    app.useGlobalPipes(
        new ValidationPipe({
            transformOptions: { enableImplicitConversion: true },
            transform: true,
            whitelist: true,
            exceptionFactory: (errors: ValidationError[]) => ({
                getStatus: () => HttpStatus.BAD_REQUEST,
                validationError: errors
            }),
        }),
    );

    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalInterceptors(new ResponseInterceptor());

    await app.listen(process.env.SERVICE_PORT);
}
bootstrap();
