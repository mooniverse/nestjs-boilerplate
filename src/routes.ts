import { Routes } from 'nest-router';
import { ProductModule } from './common/product/product.module';
import { SinglePaymentModule } from './common/single-payment/single-payment.module';
import { InquiryPostpaidModule } from './common/inquiry-postpaid/inquiry-postpaid.module';
import { PaymentPostpaidModule } from './common/payment-postpaid/payment-postpaid.module';
import { AuthenticationModule } from './shared/common/authentication/authentication.module';

const routes: Routes = [
    {
        path: '/api/auth/token',
        module: AuthenticationModule
    },
    {
        path: '/api/v3',
        children: [
            {
                path: '/product/',
                module: ProductModule,
            },
            {
                path: '/single-payment',
                module: SinglePaymentModule,
            },
            {
                path: '/inquiry-postpaid',
                module: InquiryPostpaidModule,
            },
            {
                path: '/payment-postpaid',
                module: PaymentPostpaidModule,
            },
        ],
    },
];

export default routes;
