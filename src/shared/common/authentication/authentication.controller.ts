import { Controller, Get, Headers, HttpException, HttpStatus } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { AuthenticationService } from './authentication.service';
import { } from 'express';
import { ControllerResponseDto } from 'src/shared/dto/response.dto';
import { RC } from 'src/shared/enum';
import { Users } from 'src/entities';
import { JwtDto } from 'src/shared/dto/jwt.dto';
import { LANG_EN, LANG_ID } from 'src/shared/enum/languages';

@Controller()
export class AuthenticationController {

    constructor(private readonly authenticationService: AuthenticationService, private readonly jwtService: JwtService) { }

    @Get('/')
    public async login(@Headers() headers: any): Promise<ControllerResponseDto> {

        try {

            // Get Header Basic Auth
            const authorization = headers?.authorization?.split(" ");
            if (!authorization || authorization.length < 2 || authorization?.[0] != "Basic") {
                throw Error;
            }

            // Extract Credential
            const base64Str = authorization[1];
            const decBase64Str = Buffer.from(base64Str, "base64").toString('ascii').split(":");
            const username = decBase64Str[0];
            const password = decBase64Str[1]

            // Find User From Database
            const resultUser: Users = await this.authenticationService.findUser(username);
            if (!resultUser || !username || !password) {
                throw Error;
            }

            // Check Password
            const isPasswordValid: boolean = await this.authenticationService.comparePassword(password, resultUser.password)
            if (!isPasswordValid) {
                throw Error;
            }

            // Create JWT Access Token
            const payloadToSign: JwtDto = {
                userId: resultUser.userId,
                role: resultUser.userRole.role
            }
            const accessToken: string = this.jwtService.sign(payloadToSign, { expiresIn: +process.env.JWT_EXPIRES_SEC, secret: process.env.JWT_SECRET });

            return {
                httpStatus: HttpStatus.OK,
                rc: RC.OK,
                msgID: LANG_ID.SUCCESS,
                msgEN: LANG_EN.SUCCESS,
                data: {
                    accessToken: accessToken,
                    expiresIn: +process.env.JWT_EXPIRES_SEC
                }
            }

        } catch (error) {

            throw new HttpException(
                {
                    httpStatus: HttpStatus.UNAUTHORIZED,
                    rc: RC.UNAUTHORIZED,
                    msgID: LANG_ID.UNAUTHORIZED,
                    msgEN: LANG_EN.UNAUTHORIZED,
                },
                HttpStatus.UNAUTHORIZED
            );

        }

    }

}
