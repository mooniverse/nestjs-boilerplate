import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthenticationController } from './authentication.controller';
import { AuthenticationService } from './authentication.service';
import { JwtStrategy } from 'src/shared/guards/jwt.strategy';
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { TypeOrmModule } from '@nestjs/typeorm';
import * as Entities from 'src/entities/index';

@Module({
    imports: [
        JwtModule.register({
            secret: process.env.JWT_SECRET,
            signOptions: {
                expiresIn: process.env.JWT_EXPIRES_SEC,
            },
        }),
        TypeOrmModule.forFeature([...Object.values(Entities)])
    ],
    controllers: [AuthenticationController],
    providers: [JwtAuthGuard, JwtStrategy, AuthenticationService],
})
export class AuthenticationModule { }