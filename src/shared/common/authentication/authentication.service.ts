import { Injectable, } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Users } from 'src/entities';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthenticationService {

    constructor(
        @InjectRepository(Users)
        private usersRepo: Repository<Users>,
    ) { }

    public async findUser(username: string): Promise<any> {
        return await this.usersRepo.findOne({
            where: { username: username },
            relations: {
                userRole: true
            }
        })
    }

    public async createHashPassword(plainPassword: string): Promise<string> {
        const saltOrRounds = 10;
        const hash = bcrypt.hash(plainPassword, saltOrRounds);
        return hash;
    }

    public async comparePassword(password: string, hash: string): Promise<boolean> {
        const isMatch = bcrypt.compare(password, hash);
        return isMatch;
    }

}
