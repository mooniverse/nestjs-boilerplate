export class JwtDto {
    public userId: string;
    public role: string;
    public iat?: number;
    public exp?: number;
}