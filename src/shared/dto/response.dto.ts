import { HttpStatus } from "@nestjs/common";
import { RC } from "../enum";

export class ResponseDto {
    public responseCode: RC;
    public responseMessage: responseMessageDto;
    public responseData?: object;
}

export class ControllerResponseDto {
    httpStatus: HttpStatus;
    rc: RC;
    msgID: string;
    msgEN: string;
    data?: object;
}

class responseMessageDto {
    public ID: string;
    public EN: string;
}