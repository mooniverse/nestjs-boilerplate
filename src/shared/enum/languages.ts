/**
 * Indonesian Language
 */

export enum LANG_ID {
    SUCCESS = "Sukses",
    BAD_REQUEST = "Request Tidak Valid",
    UNAUTHORIZED = "Tidak Berhak",
    FORBIDDEN = "Akses Tidak Valid",
    NOT_FOUND = "Tidak Ditemukan",
    INTERNAL_SERVER_ERROR = "Internal Error",

    // Product Module
    SUCCESS_GET_CHANNEL_PRODUCT = "Sukses Mendapatkan Produk Channel",
    SUCCES_GET_MAIN_PRODUCT = "Sukses Mendapatkan Produk Main",
    PRODUCT_NOT_FOUND = "Produk Tidak Ditemukan"
}




/**
 * English Language
 */

export enum LANG_EN {
    SUCCESS = "Success",
    BAD_REQUEST = "Invalid Request",
    UNAUTHORIZED = "Unauthorized",
    FORBIDDEN = "Invalid Access",
    NOT_FOUND = "Not Found",
    INTERNAL_SERVER_ERROR = "Internal Error",

    // Product Module
    SUCCESS_GET_CHANNEL_PRODUCT = "Success Get Channel Product",
    SUCCES_GET_MAIN_PRODUCT = "Success Get Main Product",
    PRODUCT_NOT_FOUND = "Produk Not Found"
}