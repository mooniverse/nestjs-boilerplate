import { ExceptionFilter, Catch, ArgumentsHost, HttpException, HttpStatus } from '@nestjs/common';
import { Request, Response } from 'express';
import { ResponseDto } from '../dto/response.dto';
import { RC } from '../enum';
import { LANG_EN, LANG_ID } from '../enum/languages';

@Catch()
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {

        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception?.getStatus?.() || HttpStatus.INTERNAL_SERVER_ERROR;

        if (status >= HttpStatus.INTERNAL_SERVER_ERROR) {
            console.error(exception)
        }

        const rcMapper = {
            [HttpStatus.BAD_REQUEST]            : RC.BAD_REQUEST,
            [HttpStatus.FORBIDDEN]              : RC.FORBIDDEN,
            [HttpStatus.UNAUTHORIZED]           : RC.UNAUTHORIZED,
            [HttpStatus.NOT_FOUND]              : RC.NOT_FOUND,
            [HttpStatus.INTERNAL_SERVER_ERROR]  : RC.INTERNAL_SERVER_ERROR
        }

        const msgMapper = {
            [HttpStatus.BAD_REQUEST]            : { ID: LANG_ID.BAD_REQUEST, EN: LANG_EN.BAD_REQUEST},
            [HttpStatus.FORBIDDEN]              : { ID: LANG_ID.FORBIDDEN, EN: LANG_EN.FORBIDDEN },
            [HttpStatus.UNAUTHORIZED]           : { ID: LANG_ID.UNAUTHORIZED, EN: LANG_EN.UNAUTHORIZED},
            [HttpStatus.NOT_FOUND]              : { ID: LANG_ID.NOT_FOUND, EN: LANG_EN.NOT_FOUND },
            [HttpStatus.INTERNAL_SERVER_ERROR]  : { ID: LANG_ID.INTERNAL_SERVER_ERROR, EN: LANG_EN.INTERNAL_SERVER_ERROR}
        }

        const responseStructure: ResponseDto = {
            responseCode: rcMapper?.[status] || RC.INTERNAL_SERVER_ERROR,
            responseMessage: {
                ID: msgMapper?.[status]?.["ID"] || LANG_ID.INTERNAL_SERVER_ERROR,
                EN: msgMapper?.[status]?.["EN"] || LANG_ID.INTERNAL_SERVER_ERROR
            },
            responseData: exception?.validationError || {}
        }

        return response
            .status(status)
            .json(responseStructure);
    }
} 