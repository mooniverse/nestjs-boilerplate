import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ResponseDto } from 'src/shared/dto/response.dto';
import { RC } from 'src/shared/enum';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

    public handleRequest(err: Error, user: any, info: any) {

        if (err || !user) {

            const responseData: ResponseDto = {
                responseCode: RC.UNAUTHORIZED,
                responseMessage: {
                    ID: "Tidak Memiliki Akses",
                    EN: "Unauthorized"
                },
                responseData: {},
            }

            throw new HttpException(responseData, HttpStatus.UNAUTHORIZED);

        } else {

            return user;
            
        }

    }
}
