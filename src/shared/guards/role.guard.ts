import { CanActivate, ExecutionContext, ForbiddenException, HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SetMetadata } from '@nestjs/common';
import { ResponseDto } from 'src/shared/dto/response.dto';
import { RC } from 'src/shared/enum';

export const Roles = (...roles: string[]) => SetMetadata('roles', roles);

@Injectable()
export class RoleGuard implements CanActivate {
    constructor(private reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());
        if (!roles) {
            return true;
        }
        const res = context.switchToHttp().getResponse();

        const request = context.switchToHttp().getRequest();
        const user = request.user;

        const isAllowed = roles.includes(user.role);

        if (!isAllowed) { throw new ForbiddenException } 
        else { return true }
    }
}

