import {
    CallHandler,
    ExecutionContext,
    HttpException,
    HttpStatus,
    Injectable,
    NestInterceptor,
} from '@nestjs/common';
import { catchError, map, Observable, tap } from 'rxjs';
import { ControllerResponseDto, ResponseDto } from 'src/shared/dto/response.dto';
import { RC } from '../enum';
import { LANG_EN, LANG_ID } from '../enum/languages';

@Injectable()
export class ResponseInterceptor implements NestInterceptor {
    
    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {

        return next.handle().pipe(

            map((data: ControllerResponseDto): ResponseDto => {
                
                const response = context.switchToHttp().getResponse();

                // HTTP Status >= 400
                if (data?.httpStatus >= HttpStatus.BAD_REQUEST) {
                    response.status(data?.httpStatus || HttpStatus.INTERNAL_SERVER_ERROR);
                    return {
                        responseCode: data?.rc || RC.INTERNAL_SERVER_ERROR,
                        responseMessage: {
                            ID: data?.msgID || LANG_ID.INTERNAL_SERVER_ERROR,
                            EN: data?.msgEN || LANG_EN.INTERNAL_SERVER_ERROR
                        },
                        responseData: data?.data || {},
                    }

                } 
                // HTTP Status 200
                else {
                    response.status(data?.httpStatus || HttpStatus.OK)
                    return {
                        responseCode: data?.rc || RC.OK,
                        responseMessage: {
                            ID: data?.msgID || LANG_ID.SUCCESS,
                            EN: data?.msgEN || LANG_EN.SUCCESS
                        },
                        responseData: data.data || {},
                    };

                }

            }),

            // Exception Error Handle By ExceptionFilter
            catchError((e) => {
                throw e
            })

        );

    }
}
